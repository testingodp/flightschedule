package com.travelport.schedule.fsschedule.atomic.impl;

import static org.junit.Assert.assertNotNull;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRQ;

public class FlightSpecificScheduleUtilIT {

  private FlightSpecificScheduleUtil resource = new FlightSpecificScheduleUtil();

  @Test
  public void testCallToCoreSystem() throws Exception {
    JAXBContext jaxbContext = JAXBContext.newInstance(FlightScheduleRQ.class);
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    final File file = new File("src/test/resources/mockData/FlightSpecificRequest.xml");
    FlightScheduleRQ flightScheduleRQ = (FlightScheduleRQ) jaxbUnmarshaller.unmarshal(file);
    assertNotNull(resource.retrieveAirSchedule(flightScheduleRQ, "TestTRackId", false));
  }

}
