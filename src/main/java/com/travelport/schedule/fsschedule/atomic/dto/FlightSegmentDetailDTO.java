package com.travelport.schedule.fsschedule.atomic.dto;

import java.util.List;

import org.joda.time.LocalDate;

import com.travelport.schema.common.Result;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.Flight;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightDetail;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.IntermediateStop;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.SegmentDetail;

public class FlightSegmentDetailDTO extends SegmentDetail {

  private String legNumber;

  private String totalLeg;

  private String operatingCarrier1;

  private String operatingCarrierFltNum;

  private String eteligibility;

  private String generalTrafficRestrictions;

  private Flight flight;

  private String flightFrequency;

  private LocalDate effectiveDate;

  private LocalDate discontinueDate;

  private IntermediateStop intermediateStop1;

  private IntermediateStop intermediateStop2;

  private String flightOnTimePerformanceIndicator;

  private Result result1;

  private Result result2;

  private String blackListCarrierCode;

  private List<String> classOfService;

  private FlightDetail flightDetail;

  public FlightDetail getFlightDetail() {
    return flightDetail;
  }

  public void setFlightDetail(FlightDetail flightDetail) {
    this.flightDetail = flightDetail;
  }

  public List<String> getClassOfService() {
    return classOfService;
  }

  public void setClassOfService(List<String> classOfService) {
    this.classOfService = classOfService;
  }

  public String getBlackListCarrierCode() {
    return blackListCarrierCode;
  }

  public void setBlackListCarrierCode(String blackListCarrierCode) {
    this.blackListCarrierCode = blackListCarrierCode;
  }

  public Result getResult1() {
    return result1;
  }

  public void setResult1(Result result1) {
    this.result1 = result1;
  }

  public Result getResult2() {
    return result2;
  }

  public void setResult2(Result result2) {
    this.result2 = result2;
  }

  public LocalDate getEffectiveDate() {
    return effectiveDate;
  }

  public void setEffectiveDate(LocalDate effectiveDate) {
    this.effectiveDate = effectiveDate;
  }

  public LocalDate getDiscontinueDate() {
    return discontinueDate;
  }

  public void setDiscontinueDate(LocalDate discontinueDate) {
    this.discontinueDate = discontinueDate;
  }

  public IntermediateStop getIntermediateStop1() {
    return intermediateStop1;
  }

  public void setIntermediateStop1(IntermediateStop intermediateStop1) {
    this.intermediateStop1 = intermediateStop1;
  }

  public IntermediateStop getIntermediateStop2() {
    return intermediateStop2;
  }

  public void setIntermediateStop2(IntermediateStop intermediateStop2) {
    this.intermediateStop2 = intermediateStop2;
  }

  public String getFlightFrequency() {
    return flightFrequency;
  }

  public void setFlightFrequency(String flightFrequency) {
    this.flightFrequency = flightFrequency;
  }

  public String getGeneralTrafficRestrictions() {
    return generalTrafficRestrictions;
  }

  public void setGeneralTrafficRestrictions(String generalTrafficRestrictions) {
    this.generalTrafficRestrictions = generalTrafficRestrictions;
  }

  public String getOperatingCarrierFltNum() {
    return operatingCarrierFltNum;
  }

  public void setOperatingCarrierFltNum(String operatingCarrierFltNum) {
    this.operatingCarrierFltNum = operatingCarrierFltNum;
  }

  @Override
  public Flight getFlight() {
    return flight;
  }

  public void setFlightDetail(Flight flightDetail) {
    this.flight = flightDetail;
  }

  public String getLegNumber() {
    return legNumber;
  }

  public void setLegNumber(String legNumber) {
    this.legNumber = legNumber;
  }

  public String getTotalLeg() {
    return totalLeg;
  }

  public void setTotalLeg(String totalLeg) {
    this.totalLeg = totalLeg;
  }

  public String getOperatingCarrier1() {
    return operatingCarrier1;
  }

  public void setOperatingCarrier1(String operatingCarrier1) {
    this.operatingCarrier1 = operatingCarrier1;
  }

  public String getEteligibility() {
    return eteligibility;
  }

  public void setEteligibility(String eteligibility) {
    this.eteligibility = eteligibility;
  }

  public String getFlightOnTimePerformanceIndicator() {
    return flightOnTimePerformanceIndicator;
  }

  public void setFlightOnTimePerformanceIndicator(String flightOnTimePerformanceIndicator) {
    this.flightOnTimePerformanceIndicator = flightOnTimePerformanceIndicator;
  }



}
