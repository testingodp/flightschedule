package com.travelport.schedule.fsschedule.atomic.connector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.ServiceLoader;

import javax.xml.namespace.QName;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.travelport.common.technical.logging.enterpriseloggingrecord_v1.Classifications;
import com.travelport.common.technical.logging.enterpriseloggingrecord_v1.LogLevels;
import com.travelport.esb.cib.jcp.JcpAccess;
import com.travelport.esb.cib.jcp.JcpException;
import com.travelport.esb.cib.jcp.JcpManager;
import com.travelport.odt.restfw.common.auditors.RESTInvocationAuditor;
import com.travelport.odt.restfw.common.auditors.RequestAudit;
import com.travelport.odt.restfw.common.auditors.ResponseAudit;
import com.travelport.odt.restfw.plugin.common.exceptions.GeneralServiceException;
import com.travelport.schedule.fsschedule.atomic.util.DCCResponseCompressionHandler;
import com.travelport.schedule.fsschedule.atomic.util.ErrorHandler;
import com.travelport.schedule.fsschedule.atomic.util.LogUtil;
/**
 * To Connect with DCC - JCP manager Configuration and get back the response
 * @author 
 *
 */
public class DCCConnector {

  private static final Logger LOGGER = LogManager.getLogger(DCCConnector.class);
  private static String dccPath = "DCCQRYQA";
  private static int dccTimeout =
      com.travelport.schedule.fsschedule.atomic.constants.AirScheduleAtomicConstants.CONSTANT_12;
  private static final String MULEENV = "MULE_ENV";
  private static final String MULEAPP = "MULE_APP";
  private static final String CATALOG = "catalog";
  private static Collection<RESTInvocationAuditor> auditors;
  private static final QName RESOURCE_NAME = new QName("http://www.travelport.com/DCCConnector", "DCCConnector");
  private static final String DCC_RESOURCE_SYSTEM_ID = "DCC_CONNECTOR";

  /**
   * Initialize DCC Connection 
   */
  static {
    // JCP Manager configuration for Catalog Service project
    System.setProperty(MULEAPP, CATALOG);
    // Set DCC Path value
    if (System.getenv("DCC_PATH") != null) {
      dccPath = System.getenv("DCC_PATH");
      LOGGER.info("======== DCC_PATH = " + dccPath + " ========");
    } else {
      LOGGER.error("++++++++ No DCC_PATH environment variable found ++++++++ ");
    }
    // Set DCC Path value
    if (System.getenv("DCC_TIMEOUT") != null) {
      dccTimeout = Integer.parseInt(System.getenv("DCC_TIMEOUT"));
      LOGGER.info("======== DCC_TIMEOUT = " + dccTimeout + " ========");
    } else {
      LOGGER.error("++++++++ No DCC_TIMEOUT environment variable found ++++++++ ");
    }
    // Set DCC Environment value
    if (System.getenv("DCC_ENV") != null) {
      System.setProperty(MULEENV, System.getenv("DCC_ENV"));
      LOGGER.info("======== DCC_ENV = " + System.getenv("DCC_ENV") + " ========");
    } else {
      System.setProperty(MULEENV, "dv");
      LOGGER.error("++++++++ No DCC_ENV environment variable found ++++++++ ");
    }

    try {
      JcpManager.getInstance();
    } catch (JcpException e) {
      LOGGER.error("++++++++ JcpException while instantiating JCP Manager ++++++++ ", e);
    }

    if ("true".equals(System.getenv("ENABLE_TESTFW_AUDITOR"))) {
      LOGGER.info("======== REST Invocation Auditor enabled for Test Framework ========");
      final Collection<RESTInvocationAuditor> _auditors = new ArrayList<>();
      final ServiceLoader<RESTInvocationAuditor> serviceLoader = ServiceLoader.load(RESTInvocationAuditor.class);
      final Iterator<RESTInvocationAuditor> iterator = serviceLoader.iterator();

      while (iterator.hasNext()) {
        final RESTInvocationAuditor auditor = iterator.next();
        _auditors.add(auditor);
      }
      auditors = _auditors.isEmpty() ? null : Collections.unmodifiableCollection(_auditors);
    }
  }

  public DCCConnector() {
    LOGGER.info(">>>>>>>> Instantiating DCC Connector before first request >>>>>>>>");
  }

  /**
   * Invoke DCC to get a KLR response for AirSchedules
   * 
   * @param requestKLR Request KLR to DCC
   * @param transactionID Transaction ID
   * @return Response KLR from DCC
   * @throws GeneralServiceException
   */
  public String getKLRResponse(String requestKLR, String transactionID) throws GeneralServiceException {
    auditRequest(requestKLR);
    LogUtil.sendToELog("DCC Request KLR = " + requestKLR, transactionID, Classifications.REQ_RESP, LogLevels.INFO);
    JcpAccess jcpAccess = new JcpAccess(JcpAccess.OPTMSGUNDELEX);
    try {
      final long beforeCallTime = System.currentTimeMillis();
      // Use processReq API version of JCP Manager that takes in a byte array and returns a byte
      // array.
      // This is important because when DCC returns large response back as GZip content. The string
      // version of JCPManager API corrupts the GZip response.
      // String dccKLRResponse = jcpAccess.processReq(requestKLR, DCC_PATH, 35);
      byte[] dccKLRResponseAsBytes = jcpAccess.processReq(requestKLR.getBytes(), dccPath, dccTimeout);

      final long afterCallTime = System.currentTimeMillis();

      // If the response is compressed, decompress it
      DCCResponseCompressionHandler dccResponseCompressionHandler =
          new DCCResponseCompressionHandler(dccKLRResponseAsBytes);
      String dccKLRResponseAsString = dccResponseCompressionHandler.getParseableResponse();

      LogUtil.sendToELog("DCC Response KLR = " + dccKLRResponseAsString, transactionID, Classifications.REQ_RESP,
          LogLevels.INFO);
      auditResponse(dccKLRResponseAsString, afterCallTime - beforeCallTime);
      return dccKLRResponseAsString;
    } catch (JcpException e) {
      throw ErrorHandler.handleGeneralError("JcpAccess exception", 500);
    } catch (IOException e) {
      throw ErrorHandler.handleGeneralError("IOException while decompressing DCC response", 500);
    }
  }

  /**
   * Passes information about the given request to all registered (and enabled)
   * <code>RESTInvocationAuditor</code> components.
   * 
   * @param request DCC request KLR
   */
  private void auditRequest(final String request) {
    if (auditors != null) {
      final RequestAudit audit = new RequestAudit();
      audit.setRequestUrl(dccPath);
      audit.setRequestMethod("POST");
      audit.setResourceName(RESOURCE_NAME);
      audit.setMessagePayload(request);
      audit.setSystemID(DCC_RESOURCE_SYSTEM_ID);
      audit.setContentType("text/plain");
      for (RESTInvocationAuditor auditor : auditors) {
        auditor.auditRequest(audit);
      }
    }
  }

  /**
   * Passes information about the given response to all registered (and enabled)
   * <code>RESTInvocationAuditor</code> components.
   * 
   * @param response DCC response KLR
   * @param timeElapsed
   */
  private void auditResponse(final String response, final long timeElapsed) {
    if (auditors != null) {
      final ResponseAudit audit = new ResponseAudit();
      audit.setStatusCode(200); // can't get it from here, so we will assume 200
      audit.setStatusMessage("Response Received from DCC");
      audit.setResourceName(RESOURCE_NAME);
      audit.setMessagePayload(response);
      audit.setContentType("text/plain");
      audit.setSystemID(DCC_RESOURCE_SYSTEM_ID);
      audit.setTimeElapsed(timeElapsed);
      for (RESTInvocationAuditor auditor : auditors) {
        auditor.auditResponse(audit);
      }
    }
  }

}
