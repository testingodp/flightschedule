package com.travelport.schedule.fsschedule.atomic.transformer;

import java.util.ArrayList;
import java.util.List;

import com.travelport.schema.common.DayOfWeekEnum;

public final class TransformerUtils {

  /**
   * To Map flightFrequency and eTicketingEligibility
   */
  private static final String YES = "Y";
  private static final String NO = "N";

  private static List<DayOfWeekEnum> DAY_OF_OPERATIONS = new ArrayList<DayOfWeekEnum>();

  static {
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.SUNDAY);
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.MONDAY);
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.TUESDAY);
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.WEDNESDAY);
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.THURSDAY);
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.FRIDAY);
    DAY_OF_OPERATIONS.add(DayOfWeekEnum.SATURDAY);

  }

  public static List<String> getDayOfOperationIndicator(List<DayOfWeekEnum> list) {
    List<String> dayOfOperationIndicator = new ArrayList<String>();
    if (list != null && list.size() > 0) {
      for (DayOfWeekEnum day : list) {
        if (DAY_OF_OPERATIONS.contains(day)) {
          dayOfOperationIndicator.add(YES);
        } else {
          dayOfOperationIndicator.add(NO);
        }
      }
    } else {
      dayOfOperationIndicator.add(YES);
      dayOfOperationIndicator.add(YES);
      dayOfOperationIndicator.add(YES);
      dayOfOperationIndicator.add(YES);
      dayOfOperationIndicator.add(YES);
      dayOfOperationIndicator.add(YES);
      dayOfOperationIndicator.add(YES);
    }

    return dayOfOperationIndicator;
  }

  /**
   * To populate Enum values in Flight Frequency object
   * 
   * @param dayOfOperationsIndicator
   * @return
   */
  public static List<DayOfWeekEnum> getDays(String dayOfOperationsIndicator) {
    List<DayOfWeekEnum> days = new ArrayList<DayOfWeekEnum>();
    for (int i = 0; i < 7; i++) {
      String value = Character.toString(dayOfOperationsIndicator.charAt(i));
      if (YES.equals(value)) {
        days.add(DAY_OF_OPERATIONS.get(i));
      }
    }

    return days;
  }
}
