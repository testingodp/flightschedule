package com.travelport.schedule.fsschedule.atomic.transformer;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;

import com.travelport.odt.atf.DataFormatType;
import com.travelport.odt.atf.RequestConverter;
import com.travelport.odt.atf.TransformationContext;
import com.travelport.odt.atf.TransformationException;
import com.travelport.odt.atf.binding.model.Identifier;
import com.travelport.odt.restfw.plugin.common.exceptions.GeneralServiceException;
import com.travelport.schedule.fsschedule.atomic.constants.AirScheduleAtomicConstants;
import com.travelport.schedule.fsschedule.atomic.dto.AirTravelCriteriaFlightSpecificDTO;
import com.travelport.schedule.fsschedule.atomic.util.ErrorHandler;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.AgencyPCC;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRQ;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.SpecifiedFlight;

/**
 * Transforms request object to KLR format using ATF
 * 
 * @author
 *
 */
public class FlightSpecificRequestTransformer {

  private static final Logger LOGGER = LogManager.getLogger(FlightSpecificRequestTransformer.class);
  private static TransformationContext ctx;
  static {
    try {
      // Load XML binding
      ctx = TransformationContext.newContext(AirScheduleAtomicConstants.AVAILABILITY_HOST_BINDING);
    } catch (Exception e) {
      LOGGER.error("++++++++Exception occured while loading TransformationContext ++++++++", e);
    }
  }

  /**
   * To create KLR string from Request
   * 
   * @param request
   * @param islegacy
   * @return
   * @throws GeneralServiceException
   */
  public <T> String objectToKLR(final T request, boolean islegacy) throws GeneralServiceException {
    final int spFlightCounter = getflightSegmentSize(request, islegacy);
    if (spFlightCounter > 0) {
      // Create and populate AirTravelCriteriaScheduleDTO object
      final AirTravelCriteriaFlightSpecificDTO dto = new AirTravelCriteriaFlightSpecificDTO();
      AgencyPCC agencyPCC = setAirTravelCriteriaFlightSpecificDTO(dto, request, islegacy);
      StringBuilder reqkLR = new StringBuilder();
      RequestConverter requestConverterAAP2;
      try {
        final Identifier identifierAAP2 = new Identifier(AirScheduleAtomicConstants.AAP2_KLR,
            AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
        requestConverterAAP2 =
            ctx.getRequestConverter(identifierAAP2, AirTravelCriteriaFlightSpecificDTO.class);
        String klrAAP2 = requestConverterAAP2.process(dto);
        // For time table flight specific (AAFL KLR)
        String noOfelements;
        final Identifier identifierAAFL = new Identifier(AirScheduleAtomicConstants.AAFL_KLR,
            AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
        final RequestConverter requestConverterAAFL =
            ctx.getRequestConverter(identifierAAFL, AirTravelCriteriaFlightSpecificDTO.class);
        String klrAAFL = requestConverterAAFL.process(dto);
        String klrAAP6P1 = klrAAFL.substring(0, 8);
        String klrAAP6P2 = klrAAFL.substring(11);
        if (spFlightCounter < 10) {
          noOfelements = String.format("%03d", spFlightCounter);
        } else {
          noOfelements = String.format("%02d", spFlightCounter);
        }
        klrAAFL = klrAAP6P1 + noOfelements + klrAAP6P2;

        final Identifier identifierAAPR = new Identifier(AirScheduleAtomicConstants.AAPR_KLR,
            AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
        final RequestConverter requestConverterAAPR =
            ctx.getRequestConverter(identifierAAPR, AgencyPCC.class);
        String klrAAPR = requestConverterAAPR.process(agencyPCC);

        final Identifier identifierAAAA = new Identifier(AirScheduleAtomicConstants.AAAA_KLR,
            AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
        final RequestConverter requestConverterAAAA =
            ctx.getRequestConverter(identifierAAAA, AgencyPCC.class);
        String klrAAAA = requestConverterAAAA.process(agencyPCC);
        reqkLR.append(klrAAPR);
        reqkLR.append(klrAAAA);
        reqkLR.append(klrAAP2);
        reqkLR.append(klrAAFL);
        return reqkLR.toString();
      } catch (TransformationException e) {
        LOGGER.error("++++++++ Exception while transforming AAP2 or AAP6 KLR using ATF ++++++++ ",
            e);
        throw ErrorHandler.handleGeneralError("Exception during response transformation", 500);
      } catch (Exception e) {
        LOGGER.error("++++++++ Exception while transforming AAP2 or AAP6 KLR using ATF ++++++++ ",
            e);
        throw ErrorHandler.handleGeneralError("Exception during response transformation", 500);
      }
    } else {
      throw ErrorHandler.handleGeneralError(
          "One or more mandatory element(s) not present in request - BoardPoint, OffPoint must all be present",
          400);
    }
  }

  /**
   * To populate data from request to AirTravelCriteriaScheduleDTO object
   * 
   * @param dto
   * @param request
   * @param isLegacy
   * @return
   */
  private <T> AgencyPCC setAirTravelCriteriaFlightSpecificDTO(
      AirTravelCriteriaFlightSpecificDTO dto, T request, boolean isLegacy) {
    final DateFormat dateFormat = new SimpleDateFormat(AirScheduleAtomicConstants.DATE_FORMAT);
    AgencyPCC agencyPCC = null;
    SpecifiedFlight specifiedFlight;
    List<SpecifiedFlight> specifiedFlightList;
    FlightScheduleRQ productQueryProductQueryLegacyAirFlightSpecific = (FlightScheduleRQ) request;
    agencyPCC = productQueryProductQueryLegacyAirFlightSpecific.getAgencyPCC();
    specifiedFlight = productQueryProductQueryLegacyAirFlightSpecific.getSpecifiedFlight().get(0);
    specifiedFlightList = productQueryProductQueryLegacyAirFlightSpecific.getSpecifiedFlight();
    dto.setSpecifiedFlight(specifiedFlightList);
    dto.setMaxNumberOfResults(10);
    dto.setCoreAffinity(agencyPCC.getCodeAffinity());
    setConnectionIndicator(dto, specifiedFlight);
    dto.setValidate(true);
    dto.setDaysOfOperations(TransformerUtils.getDayOfOperationIndicator(null));
    dto.setSpecific1(specifiedFlight.getDepartDate());
    setBoardOffPoint(specifiedFlight, dto, agencyPCC.getCodeAffinity());
    dto.setSystemEndDate(LocalDate.parse(dateFormat.format(new Date())).plusDays(365));
    return agencyPCC;
  }

  /**
   * To get the size of Segment flights
   * 
   * @param request
   * @param isLegacy
   * @return
   */
  private <T> int getflightSegmentSize(T request, boolean isLegacy) {
    int spFlightCounter;
    spFlightCounter = ((FlightScheduleRQ) request).getSpecifiedFlight().size();
    return spFlightCounter;

  }

  /**
   * To set the Connection Indicator
   * 
   * @param airTrvlCriteriSchedDTO
   * @param specifiedFlight
   */
  private void setConnectionIndicator(AirTravelCriteriaFlightSpecificDTO airTrvlCriteriSchedDTO,
      SpecifiedFlight specifiedFlight) {
    if (specifiedFlight.isConnectionInd()) {
      airTrvlCriteriSchedDTO.setConnectionInd1(true);
      airTrvlCriteriSchedDTO.setConnectionInd2(true);
      airTrvlCriteriSchedDTO.setConnectionInd3(true);
    }
  }

  /**
   * To set the board and off point
   * 
   * @param specifiedFlight
   * @param airTrvlCriteriSchedDTO
   * @param coreAffinity
   */
  private void setBoardOffPoint(SpecifiedFlight specifiedFlight,
      AirTravelCriteriaFlightSpecificDTO airTrvlCriteriSchedDTO, String coreAffinity) {
    airTrvlCriteriSchedDTO.setRequestedBoardPoint(specifiedFlight.getBoardPoint().getValue());
    airTrvlCriteriSchedDTO.setRequestedOffPoint(specifiedFlight.getOffPoint().getValue());
    // Board point & Off point logic for last part of KLR(currently this
    // mapping is done for 1G only)
    airTrvlCriteriSchedDTO.setBrdPoint("D");
    airTrvlCriteriSchedDTO.setOfPoint("D");
  }


}
