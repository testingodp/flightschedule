package com.travelport.schedule.fsschedule.atomic.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DCCResponseCompressionHandler {

  /**
   * The header bytes if we are getting a compressed response.
   */
  // Character set does not matter as we are in the low ASCII range
  private static final byte[] LZ77_HEADER = "LZ77".getBytes();

  /**
   * Log for the class
   */
  private static final Logger LOGGER = LogManager.getLogger(DCCResponseCompressionHandler.class);

  /**
   * The raw response from DCC
   */
  private final byte[] rawResponse;

  /**
   * Creates a new compression handler with the raw DCC response.
   * 
   * @param rawResponse The raw response from DCC
   */
  public DCCResponseCompressionHandler(byte[] rawResponse) {
    this.rawResponse = rawResponse;
  }

  /**
   * Checks if the response is
   * 
   * @return <code>true</code> if the response is compressed
   */
  public boolean isCompressed() {
    boolean isCompressed = true;

    if (rawResponse.length < LZ77_HEADER.length) {
      isCompressed = false;
    } else {
      // Compare the first few bytes
      for (int i = 0; i < LZ77_HEADER.length; i++) {
        if (LZ77_HEADER[i] != rawResponse[i]) {
          isCompressed = false;
          break;
        }
      }
    }

    return isCompressed;
  }

  /**
   * Return the parseable format of the response
   * 
   * @return The String response to parse
   * @throws IOException IOException reading or decompressing data
   */
  public String getParseableResponse() throws IOException {
    if (!isCompressed()) {
      // For now assuming everything is low ASCII, so CHARSET does not matter
      return new String(rawResponse);
    } else {
      if (LOGGER.isDebugEnabled()) {
        LOGGER.debug("Compressed response... decompressing");
      }

      return decompressResponse();
    }
  }

  /**
   * Inflates the response
   * 
   * @return The decompressed response
   * @throws IOException
   */
  private String decompressResponse() throws IOException {
    final ByteArrayInputStream compressedData = new ByteArrayInputStream(rawResponse, 16, rawResponse.length - 16);
    final GZIPInputStream gzipInputStream = new GZIPInputStream(compressedData);

    final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    final byte[] buf = new byte[1024];
    int len = 0;
    while ((len = gzipInputStream.read(buf)) != -1) {
      baos.write(buf, 0, len);
    }
    final String decompressedResponse = new String(baos.toByteArray());
    return decompressedResponse;
  }

}
