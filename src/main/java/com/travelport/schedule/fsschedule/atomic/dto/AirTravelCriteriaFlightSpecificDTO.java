package com.travelport.schedule.fsschedule.atomic.dto;

import java.util.List;

import org.joda.time.LocalDate;

import com.travelport.training.od_bootcamp.tcs.mp3.resource.SpecifiedFlight;



public class AirTravelCriteriaFlightSpecificDTO {


  private List<SpecifiedFlight> specifiedFlights;

  private String requestedBoardPoint;

  private String requestedOffPoint;

  private String brdPoint;

  private String ofPoint;

  private String boardPoint1;

  private String offPoint1;

  private LocalDate specific1;

  private LocalDate systemEndDate;

  private boolean connectionInd;

  private boolean connectionInd1;

  private boolean connectionInd2;

  private boolean connectionInd3;

  private String airlinesIncExcEnum;

  private List<String> daysOfOperations;

  private boolean validate;

  public boolean isValidate() {
    return validate;
  }

  public void setValidate(boolean validate) {
    this.validate = validate;
  }

  public List<SpecifiedFlight> getSpecifiedFlight() {
    return specifiedFlights;
  }

  public void setSpecifiedFlight(List<SpecifiedFlight> specifiedFlight) {
    this.specifiedFlights = specifiedFlight;
  }

  public String getRequestedBoardPoint() {
    return requestedBoardPoint;
  }

  public void setRequestedBoardPoint(String requestedBoardPoint) {
    this.requestedBoardPoint = requestedBoardPoint;
  }

  public String getRequestedOffPoint() {
    return requestedOffPoint;
  }

  public void setRequestedOffPoint(String requestedOffPoint) {
    this.requestedOffPoint = requestedOffPoint;
  }

  public String getBrdPoint() {
    return brdPoint;
  }

  public void setBrdPoint(String brdPoint) {
    this.brdPoint = brdPoint;
  }

  public String getOfPoint() {
    return ofPoint;
  }

  public void setOfPoint(String ofPoint) {
    this.ofPoint = ofPoint;
  }

  public String getBoardPoint1() {
    return boardPoint1;
  }

  public void setBoardPoint1(String boardPoint1) {
    this.boardPoint1 = boardPoint1;
  }

  public String getOffPoint1() {
    return offPoint1;
  }

  public void setOffPoint1(String offPoint1) {
    this.offPoint1 = offPoint1;
  }

  public LocalDate getSpecific1() {
    return specific1;
  }

  public void setSpecific1(LocalDate specific1) {
    this.specific1 = specific1;
  }

  public LocalDate getSystemEndDate() {
    return systemEndDate;
  }

  public void setSystemEndDate(LocalDate systemEndDate) {
    this.systemEndDate = systemEndDate;
  }

  public boolean isConnectionInd() {
    return connectionInd;
  }

  public void setConnectionInd(boolean connectionInd) {
    this.connectionInd = connectionInd;
  }

  public boolean isConnectionInd1() {
    return connectionInd1;
  }

  public void setConnectionInd1(boolean connectionInd1) {
    this.connectionInd1 = connectionInd1;
  }

  public boolean isConnectionInd2() {
    return connectionInd2;
  }

  public void setConnectionInd2(boolean connectionInd2) {
    this.connectionInd2 = connectionInd2;
  }

  public boolean isConnectionInd3() {
    return connectionInd3;
  }

  public void setConnectionInd3(boolean connectionInd3) {
    this.connectionInd3 = connectionInd3;
  }

  public String getAirlinesIncExcEnum() {
    return airlinesIncExcEnum;
  }

  public void setAirlinesIncExcEnum(String airlinesIncExcEnum) {
    this.airlinesIncExcEnum = airlinesIncExcEnum;
  }

  public List<String> getDaysOfOperations() {
    return daysOfOperations;
  }

  public void setDaysOfOperations(List<String> daysOfOperations) {
    this.daysOfOperations = daysOfOperations;
  }

  public String getCoreAffinity() {
    return coreAffinity;
  }

  public void setCoreAffinity(String coreAffinity) {
    this.coreAffinity = coreAffinity;
  }

  public Integer getMaxNumberOfResults() {
    return maxNumberOfResults;
  }

  public void setMaxNumberOfResults(Integer maxNumberOfResults) {
    this.maxNumberOfResults = maxNumberOfResults;
  }

  private String coreAffinity;

  private Integer maxNumberOfResults;

}
