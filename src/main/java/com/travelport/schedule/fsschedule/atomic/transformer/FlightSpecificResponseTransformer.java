package com.travelport.schedule.fsschedule.atomic.transformer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.Period;

import com.travelport.odt.atf.DataFormatType;
import com.travelport.odt.atf.ResponseConverter;
import com.travelport.odt.atf.TransformationContext;
import com.travelport.odt.atf.TransformationException;
import com.travelport.odt.atf.binding.model.Identifier;
import com.travelport.odt.atf.util.KLRUtil;
import com.travelport.odt.restfw.plugin.common.exceptions.GeneralServiceException;
import com.travelport.schedule.fsschedule.atomic.constants.AirScheduleAtomicConstants;
import com.travelport.schedule.fsschedule.atomic.dto.FlightSegmentDetailDTO;
import com.travelport.schedule.fsschedule.atomic.util.ErrorHandler;
import com.travelport.schema.common.ResultStatusEnum;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.Arrival;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.Departure;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.Flight;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightDetail;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRS;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.ObjectFactory;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.Segment;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.SegmentDetail;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.SegmentID;

/**
 * To Convert the response KLR to AirAvailabilityRS Object
 * 
 * @author
 *
 */
public class FlightSpecificResponseTransformer {
  private static final Logger LOGGER =
      LogManager.getLogger(FlightSpecificResponseTransformer.class);
  private static TransformationContext ctx;
  private static final ObjectFactory objectFactory = new ObjectFactory();

  static {
    try {
      // Load XML binding
      ctx = TransformationContext.newContext(AirScheduleAtomicConstants.AVAILABILITY_HOST_BINDING);
    } catch (Exception e) {
      LOGGER.error("++++++++Exception occured while loading TransformationContext ++++++++", e);
    }
  }

  /**
   * To create response from the KLR
   * 
   * @param hostResponse
   * @param isLegacy
   * @return
   * @throws GeneralServiceException
   */
  public FlightScheduleRS klrToObject(String hostResponse, boolean isLegacy)
      throws GeneralServiceException {
    FlightScheduleRS availabilityRs = new FlightScheduleRS();
    com.travelport.schema.common.Identifier responseIdentifier =
        new com.travelport.schema.common.Identifier();
    responseIdentifier.setValue(UUID.randomUUID().toString());
    availabilityRs.setIdentifier(responseIdentifier);
    try {
      if (hostResponse != null && hostResponse.length() > 0) {
        if (hostResponse.charAt(0) == '[') {
          hostResponse = hostResponse.substring(1);
        }
        if (hostResponse.charAt((hostResponse.length() - 1)) == ']') {
          hostResponse = hostResponse.substring(0, (hostResponse.length() - 1));
        }
        List<String> klrList = KLRUtil.extractKLRsAsListFromRecord(hostResponse);
        if (null != klrList && !klrList.isEmpty()) {
          return mapKLRToObject(klrList, availabilityRs, isLegacy);
        }
      } else {
        LOGGER.error("++++++++ DCC response is blank ++++++++");
      }
    } catch (GeneralServiceException e) {
      throw e;
    } catch (Exception e) {
      LOGGER.error("++++++++ Exception during response transformation ++++++++ ", e);
      throw ErrorHandler.handleGeneralError("Exception during response transformation", 500);
    }

    return availabilityRs;
  }

  /**
   * To map the response KLR to AirAvailabilityRS Object
   * 
   * @param klrList
   * @param airScheduleList
   * @param availabilityRs
   * @param isLegacy
   * @return
   * @throws GeneralServiceException
   */
  private FlightScheduleRS mapKLRToObject(List<String> klrList, FlightScheduleRS availabilityRs,
      boolean isLegacy) throws GeneralServiceException {
    List<FlightSegmentDetailDTO> temFlightSegmentList = new ArrayList<>();
    int totalLeg = 0;
    int legNumber = 0;
    int flightCounter = 1;
    int index = 0;

    for (String dataFragment : klrList) {
      if (dataFragment.contains(AirScheduleAtomicConstants.AAPE_KLR)) {
        FlightSegmentDetailDTO airSegmentDetailDTO =
            createAirSegDetailDTO(dataFragment, flightCounter);
        totalLeg = Integer.parseInt(airSegmentDetailDTO.getTotalLeg());
        legNumber = Integer.parseInt(airSegmentDetailDTO.getLegNumber());
        airSegmentDetailDTO.setId("" + legNumber);
        if (totalLeg > 1 && legNumber == 1) {
        }

        if (totalLeg > legNumber) {
          temFlightSegmentList.add(airSegmentDetailDTO);
        } else {
          temFlightSegmentList.add(airSegmentDetailDTO);
          availabilityRs = createAirAvailability(temFlightSegmentList, isLegacy);
          temFlightSegmentList = new ArrayList<FlightSegmentDetailDTO>();
        }
      } else if (dataFragment.contains(AirScheduleAtomicConstants.AAD3_KLR)) {
        index = 0;
      } else if (dataFragment.contains(AirScheduleAtomicConstants.AAON_KLR)) {
        Flight flight = extractAAONKLRtoObject(dataFragment, new Flight());
      } else if (dataFragment.contains(AirScheduleAtomicConstants.AAPD_KLR)) {
        addInfotoAirSchedule(dataFragment);
      } else if (dataFragment.contains(AirScheduleAtomicConstants.AABL_KLR)) {

      }
    }
    return availabilityRs;

  }


  /**
   * To convert AAON KLR from DCC response to FlightSegmentDetailDTO object
   * 
   * @param dataFragment
   * @param flight
   * @return
   */
  private Flight extractAAONKLRtoObject(String dataFragment, Flight flight) {

    Identifier identifierForAS = new Identifier(AirScheduleAtomicConstants.AAON_KLR,
        AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
    try {
      ResponseConverter responseConverterForAS =
          ctx.getResponseConverter(identifierForAS, FlightSegmentDetailDTO.class);
      FlightSegmentDetailDTO temObj =
          (FlightSegmentDetailDTO) responseConverterForAS.process(dataFragment);
      return temObj.getFlight();
    } catch (Exception e) {
      LOGGER.error("++++++++ Exception while transforming AAON KLR using ATF ++++++++ ", e);
      ErrorHandler.handleGeneralError("Exception while transforming AAON KLR using ATF",
          AirScheduleAtomicConstants.ERROR_CODE_500);
    }
    return flight;
  }


  /**
   * To Calculate the Lay Over Period
   * 
   * @param inboundFlight
   * @param outboundFlight
   * @return
   */
  private Period calculateDuration(FlightSegmentDetailDTO inboundFlight,
      FlightSegmentDetailDTO outboundFlight) {
    Period difference = new Period();
    LocalDate arrivalDate;
    LocalTime defaultTime = new LocalTime(23, 59, 00);
    LocalTime arrivalTime = inboundFlight.getArrival().getTime();
    LocalTime depatureTime = outboundFlight.getDeparture().getTime();
    if (inboundFlight.getArrival().getDate() != null) {
      arrivalDate = inboundFlight.getArrival().getDate();
    } else {
      arrivalDate = inboundFlight.getDeparture().getDate();
    }
    LocalDate depatureDate = outboundFlight.getDeparture().getDate();
    if (depatureDate.getDayOfMonth() == arrivalDate.getDayOfMonth()) {
      difference = Period.fieldDifference(arrivalTime, depatureTime);
    } else {
      difference = Period.fieldDifference(arrivalTime, defaultTime);
      difference = difference.plusHours(depatureTime.getHourOfDay());
      difference = difference.plusMinutes(depatureTime.getMinuteOfHour());
    }
    return difference;
  }

  /**
   * This method creates AirAvailability from host response
   * 
   * @param flightSegmentDetailsList
   * @param isLegacy
   * @return
   */
  private FlightScheduleRS createAirAvailability(
      List<FlightSegmentDetailDTO> flightSegmentDetailsList, boolean isLegacy) {
    FlightScheduleRS resp = new FlightScheduleRS();
    List<SegmentID> flightSegmentList = new ArrayList<>();
    Period segmentsDuration = new Period();
    for (FlightSegmentDetailDTO dto : flightSegmentDetailsList) {
      Segment flightSegment;
      if (!isLegacy) {
        flightSegment = new SegmentDetail();
      } else {
        flightSegment = new Segment();
      }
      // Flight
      flightSegment.setFlight(createFlight(new Flight(), dto));
      flightSegment.setId(dto.getId());
      // Departure
      createDeparture(flightSegment, dto.getDeparture());
      // Arrival
      createArrival(flightSegment, dto.getArrival());
      // Number of Stops
      flightSegment.setStops(dto.getStops());
      // duration
      flightSegment.setDuration(dto.getDuration());
      segmentsDuration = addDuration(segmentsDuration, dto.getDuration());
      flightSegmentList.add(((SegmentDetail) flightSegment));
    }
    resp.setSegment(flightSegmentList);
    com.travelport.schema.common.Identifier productIdentifier =
        new com.travelport.schema.common.Identifier();
    productIdentifier.setValue(UUID.randomUUID().toString());
    resp.setIdentifier(productIdentifier);
    return resp;
  }

  /**
   * To populate all Flight Details from DTO to AirAvailabilityRS
   * 
   * @param flightSegment
   * @param flightDto
   * @param flightOnTimePerformanceIndicator
   * @param eteligibility
   * @param flightFrequency
   */
  private void createFlightDetail(Segment flightSegment, FlightDetail flightDto,
      String flightOnTimePerformanceIndicator, String eteligibility, String flightFrequency) {
    FlightDetail flightDetail = new FlightDetail();
    // FlightFrequency
    flightDetail.setFlightFrequency(TransformerUtils.getDays(flightFrequency));
    // EffectiveDate
    flightDetail.setEffectiveDate(flightDto.getEffectiveDate());
    // DisContinueDate
    flightDetail.setDiscontinueDate(flightDto.getDiscontinueDate());
    // codeShareSrcInd
    flightDetail.setCodeShareSrcInd(flightDto.isCodeShareSrcInd());
    // changeOfGaugeInd
    flightDetail.setChangeOfGaugeInd(flightDto.isChangeOfGaugeInd());
    // flightOnTimePerformanceIndicator
    // operatingCarrierFltNum
    if (null != flightDto.getOperatingCarrierFltNum()
        && flightDto.getOperatingCarrierFltNum().trim().length() != 0
        && !"0000".equals(flightDto.getOperatingCarrierFltNum())) {
      flightDetail.setOperatingCarrierFltNum(flightDto.getOperatingCarrierFltNum());
    }
    (flightSegment).setFlight(flightDetail);
  }

  /**
   * To populate arrival details from DTO to AirAvailabilityRS
   * 
   * @param flightSegment
   * @param arrival
   */
  private void createArrival(Segment flightSegment, Arrival arrival) {
    // Arrival
    flightSegment.setArrival(arrival);
    // Arrival Terminal
    if (arrival != null) {
      if (arrival instanceof Arrival) {
        if (((Arrival) arrival).getTerminal().trim().length() != 0) {
          ((Arrival) flightSegment.getArrival()).setTerminal(((Arrival) arrival).getTerminal());
        }
      }
      // Arrival Time
      if (arrival.getTime() != null) {
        flightSegment.getArrival().setTime(arrival.getTime());
      }
      // Arrival Airport
      if (arrival.getLocation() != null) {
        flightSegment.getArrival().setLocation(arrival.getLocation());
      }
    }
  }

  /**
   * To populate the Departure details from DTO to AirAvailabilityRS
   * 
   * @param flightSegment
   * @param departure
   */
  private void createDeparture(Segment flightSegment, Departure departure) {
    // Departure
    flightSegment.setDeparture(departure);
    // Departure Terminal
    if (departure != null) {
      if (departure instanceof Departure
          && ((Departure) departure).getTerminal().trim().length() != 0) {
        ((Departure) flightSegment.getDeparture())
            .setTerminal(((Departure) departure).getTerminal());
      }
      // Departure Time
      if (departure.getTime() != null) {
        flightSegment.getDeparture().setTime(departure.getTime());
      }
      // Departure Airport
      if (departure.getLocation() != null) {
        flightSegment.getDeparture().setLocation(departure.getLocation());
      }
    }
  }

  /**
   * To populate all Flight Details from DTO to AirAvailabilityRS
   * 
   * @param flight
   * @param dto
   * @return
   */
  private Flight createFlight(Flight flight, FlightSegmentDetailDTO dto) {
    // Flight Number
    flight.setNumber(dto.getFlight().getNumber());

    // Vendor Code
    flight.setCarrier(dto.getFlight().getCarrier());

    // Equipment Code
    if (null != dto.getFlight().getEquipment()
        && dto.getFlight().getEquipment().trim().length() != 0) {
      flight.setEquipment(dto.getFlight().getEquipment());
    }
    // Operating Carrier
    if (null != dto.getFlight().getOperatingCarrier()
        && dto.getFlight().getOperatingCarrier().trim().length() != 0) {
      flight.setOperatingCarrier(dto.getFlight().getOperatingCarrier());
      // OperatingCarrierName from AAON
    }
    return flight;
  }


  /**
   * To add durations of segments in ProductAirAvailability
   * 
   * @param segmentsDuration
   * @param duration
   * @return
   */
  private Period addDuration(Period segmentsDuration, Period duration) {
    return segmentsDuration.plus(duration);
  }

  /**
   * Map AAPE KLR using ATF
   * 
   * @param dataFragment data
   * @param flightCounter flight counter
   * @return FlightSegmentDetailDTO
   * @throws GeneralServiceException
   */
  private FlightSegmentDetailDTO createAirSegDetailDTO(String dataFragment, int flightCounter)
      throws GeneralServiceException {
    FlightSegmentDetailDTO flightSegmentDetailDTO = new FlightSegmentDetailDTO();
    flightSegmentDetailDTO.setId("" + flightCounter);

    Identifier identifier = new Identifier(AirScheduleAtomicConstants.AAPE_KLR,
        AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
    try {
      ResponseConverter responseConverter =
          ctx.getResponseConverter(identifier, FlightSegmentDetailDTO.class);
      flightSegmentDetailDTO = (FlightSegmentDetailDTO) responseConverter.process(dataFragment);
    } catch (TransformationException e) {
      LOGGER.error("++++++++ Exception while transforming AAPE KLR using ATF ++++++++ ", e);
      throw ErrorHandler.handleGeneralError("Exception while transforming AAPE KLR using ATF", 500);
    } catch (Exception e) {
      LOGGER.error("++++++++ Exception while transforming AAPE KLR using ATF ++++++++ ", e);
      throw ErrorHandler.handleGeneralError("Exception while transforming AAPE KLR using ATF", 500);
    }

    return flightSegmentDetailDTO;
  }

  /**
   * To Convert AAPD KLR to FlightSegmentDetailDTO using ATF
   * 
   * @param dataFragment data
   * @param flightCounter flight counter
   * @return FlightSegmentDetailDTO
   * @throws GeneralServiceException
   */
  private void addInfotoAirSchedule(String dataFragment) throws GeneralServiceException {
    Identifier identifierForAS = new Identifier(AirScheduleAtomicConstants.AAPD_KLR,
        AirScheduleAtomicConstants.KLR_VERSION_01, DataFormatType.KLR);
    FlightSegmentDetailDTO temObj = null;
    try {
      ResponseConverter responseConverterForAS =
          ctx.getResponseConverter(identifierForAS, FlightSegmentDetailDTO.class);
      temObj = (FlightSegmentDetailDTO) responseConverterForAS.process(dataFragment);
    } catch (Exception e) {
      LOGGER.error("++++++++ Exception while transforming AAPD KLR using ATF ++++++++ ", e);
      throw ErrorHandler.handleGeneralError("Exception while transforming AAPD KLR using ATF", 500);
    }
    if (ResultStatusEnum.INCOMPLETE.equals(temObj.getResult1().getStatus())) {
      throw ErrorHandler.handleGeneralError("Error Finding Flight Items for 1 Leg", 500);
    }
  }


}
