package com.travelport.schedule.fsschedule.atomic.impl;

import javax.ws.rs.core.HttpHeaders;

import com.travelport.odt.restfw.plugin.common.exceptions.GeneralServiceException;
import com.travelport.schedule.fsschedule.atomic.connector.DCCConnector;
import com.travelport.schedule.fsschedule.atomic.transformer.FlightSpecificRequestTransformer;
import com.travelport.schedule.fsschedule.atomic.transformer.FlightSpecificResponseTransformer;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRQ;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRS;

public class FlightSpecificScheduleUtil {

  private static final FlightSpecificRequestTransformer REQUESTTRANSFORMER =
      new FlightSpecificRequestTransformer();
  private static final FlightSpecificResponseTransformer RESPONSETRANSFORMER =
      new FlightSpecificResponseTransformer();
  private static final DCCConnector CONNECTOR = new DCCConnector();

  /**
   * To retrieve Scheduled flight details from the DCC against incoming flight specific criteria
   * 
   * @param maxNumberResults
   * @param resultsPerPage
   * @param flightSpecificCriteria
   * @param agencyPCC
   * @param e2eTrackingId
   * @return
   * @throws GeneralServiceException
   */
  
  /*This method is used for normal interaction with DCC library*/
  public FlightScheduleRS retrieveAirSchedule(FlightScheduleRQ productQueryFlightSpecific,
	      String e2eTrackingId, boolean islegacy) throws GeneralServiceException {
	    final String dccRequest = REQUESTTRANSFORMER.objectToKLR(productQueryFlightSpecific, islegacy);
	    final String dccResponse = CONNECTOR.getKLRResponse(dccRequest, e2eTrackingId);
	    return RESPONSETRANSFORMER.klrToObject(dccResponse, islegacy);
  }
  
  /*This method is used for TAF RECORD feature*/
  public FlightScheduleRS retrieveAirSchedule(FlightScheduleRQ productQueryFlightSpecific,
      String e2eTrackingId, boolean islegacy, HttpHeaders headers) throws GeneralServiceException {
    CoreConnectorImpl impl = new CoreConnectorImpl();
    String dccRequest = REQUESTTRANSFORMER.objectToKLR(productQueryFlightSpecific, false);
    String dccResponse = null;
    String mode = headers.getHeaderString("auditheader_rcpb_mode");
    switch (mode) {
      case "RECORD":
        dccResponse = CONNECTOR.getKLRResponse(dccRequest, e2eTrackingId);
        impl.record(dccRequest, dccResponse, headers, "DCC");
        break;
      case "PLAYBACK":
        break;
      default:
        dccResponse = CONNECTOR.getKLRResponse(dccRequest, e2eTrackingId);
        break;
    }
    return RESPONSETRANSFORMER.klrToObject(dccResponse, islegacy);

  }


}
