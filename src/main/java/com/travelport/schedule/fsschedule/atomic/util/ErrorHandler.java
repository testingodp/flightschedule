package com.travelport.schedule.fsschedule.atomic.util;

import java.util.List;

import com.travelport.odt.restfw.plugin.common.exceptions.GeneralServiceException;
import com.travelport.odt.restfw.plugin.common.exceptions.NotAuthorizedException;
import com.travelport.schema.common.BaseResponse;
import com.travelport.schema.common.Error;
import com.travelport.schema.common.Result;
import com.travelport.schema.common.ResultStatusEnum;

public class ErrorHandler {

  private ErrorHandler() {}

  /**
   * Builds a {@link NotAuthorizedException} and attaches an error result with the provided message.
   * 
   * @param message The error message
   * @return The NotAuthorizedException
   */
  public static NotAuthorizedException handleNotAuthorized(String message) {
    final Result result = buildResult(message);
    return new NotAuthorizedException(result);
  }

  /**
   * Builds a general service exception with the provided code and attaches a result with the
   * provided message.
   * 
   * @param message The error message
   * @param code The HTTP error code to be returned.
   * @return The general service exception
   */
  public static GeneralServiceException handleGeneralError(String message, Integer code) {
    Result result = buildResult(message);
    return new GeneralServiceException(code, result);
  }

  /**
   * Builds a general service exception with the provided code and attaches a result with the
   * provided message.
   * 
   * @param message The error message
   * @param baseResponse
   * @param code The HTTP error code to be returned.
   * @return The general service exception
   */
  public static GeneralServiceException handleEmptyResponseError(String message,
      BaseResponse baseResponse, Integer code) {
    Result result = buildResult(message);
    result.setStatus(ResultStatusEnum.INCOMPLETE);
    return new GeneralServiceException(code, baseResponse);
  }

  /**
   * Build the result object for a given fatal exception.
   * 
   * @param message The message to be returned in the result
   * @return The result object
   */
  private static Result buildResult(String message) {
    Result result = new Result();
    final Error error = new Error();
    error.setMessage(message);
    result.getError().add(error);
    return result;
  }

  /**
   * Build the result object for a given fatal exception.
   * 
   * @param message The message to be returned in the result
   * @param ResultStatusEnum the Result status
   * @return The result object
   */
  public static Result buildResult(String message, ResultStatusEnum resultStatusEnum) {
    Result result = new Result();
    result.setStatus(resultStatusEnum);
    final Error error = new Error();
    error.setMessage(message);
    result.getError().add(error);
    return result;
  }

  /**
   * Build the Error object for a validation or system error.
   * 
   * @param message The message to be returned in the Error
   * @return The Error object
   */
  public static Error buildError(String message) {
    final Error error = new Error();
    error.setMessage(message);
    return error;
  }

  /**
   * Build the Result object.
   * 
   * @param errors list of Error objects
   * @return Result
   */
  public static Result buildResult(List<Error> errors) {
    Result result = new Result();
    result.setError(errors);
    return result;
  }

}
