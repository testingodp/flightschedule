package com.travelport.schedule.fsschedule.atomic.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.travelport.common.technical.logging.enterpriseloggingrecord_v1.Classifications;
import com.travelport.common.technical.logging.enterpriseloggingrecord_v1.LogLevels;
import com.travelport.soa.lib.common.technical.logging.client.EnterpriseLoggingLogManager;
import com.travelport.soa.lib.common.technical.logging.client.EnterpriseLoggingLogger;
import com.travelport.soa.lib.common.technical.logging.client.EnterpriseLoggingRecordBuilder;

public class LogUtil {
  private static final Logger LOGGER = LogManager.getLogger(LogUtil.class);

  /**
   * Sends request, response, or other messages to e-logging system.
   * 
   * @param msg
   * @param trackId
   * @param classification
   * @param logLevel
   */
  public static void sendToELog(String msg, String trackId, Classifications classification, LogLevels logLevel) {
    long timeOfLog = System.currentTimeMillis();
    EnterpriseLoggingRecordBuilder eLogRecord = new EnterpriseLoggingRecordBuilder();
    eLogRecord.setClassification(classification);
    eLogRecord.setLogLevel(logLevel);
    eLogRecord.setSvcComponent("AIRAVAILABILITYATOMIC");
    eLogRecord.setSvcDomain("AIRAVAILABILITYATOMIC");
    eLogRecord.setSvcId("AirAvailabilityAtomicService");
    eLogRecord.setSvcFunct("retrieveAirSchedule");
    eLogRecord.setSvcInstId("AirAvailabilityAtomicService");
    eLogRecord.setSvcIpAddress("");
    eLogRecord.setSvcPayloadVer("0.0");
    eLogRecord.setSvcVer("1.0");
    eLogRecord.setSvcEventTs(timeOfLog);
    eLogRecord.setLogMessage(msg);
    eLogRecord.setTrackingId(trackId);

    EnterpriseLoggingLogger epLogger = EnterpriseLoggingLogManager.getElogger();
    epLogger.log(eLogRecord.build());
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug(eLogRecord);
    }
  }

}
