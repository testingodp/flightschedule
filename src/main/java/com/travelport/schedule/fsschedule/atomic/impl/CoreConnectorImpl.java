package com.travelport.schedule.fsschedule.atomic.impl;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.xml.namespace.QName;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.travelport.odt.restfw.common.FrameworkConstants;
import com.travelport.odt.restfw.consumer.InvocationManager;
import com.travelport.odt.restfw.consumer.InvocationManagerFactory;
import com.travelport.odt.restfw.consumer.exceptions.InvocationException;
import com.travelport.odt.restfw.plugin.common.exceptions.ServiceException;
import com.travelport.taf.rcpb.CoreConnector;
import com.travelport.taf.scenario.TAFScenarioConstants;
import com.tvp.model.tafservice.AbstractTAFRecordPlaybackResource;
import com.tvp.model.tafservice.RCPBModeEnum;
import com.tvp.model.tafservice.TAFRecordPlaybackIdentifier;
import com.tvp.model.tafservice.TAFRecordPlaybackSummary;

public class CoreConnectorImpl implements CoreConnector {
  private static InvocationManager invocationManager = InvocationManagerFactory.getManager();
  private static QName resource =
      new QName(AbstractTAFRecordPlaybackResource.NAMESPACE_V2, "TAFRecordPlaybackResource");
  public static final Logger LOGGER = LoggerFactory.getLogger(CoreConnectorImpl.class);

  @Override
  public void record(String reqPayLoad, String resPayLoad, HttpHeaders headers,
      String coreSystemId) {
    DateTime createTimeStamp = new DateTime();
    TAFRecordPlaybackSummary tafRecordSummary =
        createTafTestRecordPlaybackData(reqPayLoad, createTimeStamp, resPayLoad, coreSystemId);
    try {
      invocationManager.invocationBuilder(headers).resource(resource).action("Record")
          .systemID(TAFScenarioConstants.SYSTEM_ID_RCPB)
          .header(FrameworkConstants.CONTENT_TYPE_HEADER, MediaType.APPLICATION_XML)
          .header(FrameworkConstants.ACCEPT_VERSION_HEADER, 2)
          .header(FrameworkConstants.CONTENT_VERSION_HEADER, 2)
          .header(TAFScenarioConstants.AUDITHEADER_URL_NAME, TAFScenarioConstants.DUMMY_URL)
          .header(TAFScenarioConstants.AUDITHEADER_RCPB_MODE, RCPBModeEnum.RECORD.toString())
          .payload(tafRecordSummary).build().invoke(TAFRecordPlaybackIdentifier.class);
    } catch (InvocationException | ServiceException e) {
      LOGGER.error("Invocation Exception Failed");
    }
  }

  @Override
  public void playback(HttpHeaders headers) {

  }

  protected static TAFRecordPlaybackSummary createTafTestRecordPlaybackData(String reqpayload,
      DateTime createTimeStamp, String respPayload, String coreSystemId) {
    TAFRecordPlaybackSummary tafRecordPlaybackSummary = new TAFRecordPlaybackSummary();
    tafRecordPlaybackSummary.setRequestPayload(reqpayload);
    tafRecordPlaybackSummary.setCreateTimestamp(createTimeStamp);
    tafRecordPlaybackSummary.setResponsePayload(respPayload);
    tafRecordPlaybackSummary.setCoreSystemId(coreSystemId);
    return tafRecordPlaybackSummary;

  }

  /**
   * Core system call - connect to core system and retrieve response
   */
  @Override
  public String invokeCore(String coreRequest, String coreResponse) {
    return "SRD0163aa9f4A000FPRR01000101Y00DOT012025072900772K00000001000101CTRL010F000100010BP100001BP120001IT010001BP200001BP160001";
  }
}
