package com.travelport.schedule.fsschedule.atomic.constants;

public class AirScheduleAtomicConstants {

  public static final String E2ETrackingID_KEY = "E2ETrackingID";
  public static final String CORE_AFFINITY = "CoreAffinity";
  public static final String CORE_1P = "1P";
  public static final String CORE_1G = "1G";
  public static final String CORE_1V = "1V";
  public static final String AAPD_KLR = "AAPD";
  public static final String AAPE_KLR = "AAPE";
  public static final String AAP2_KLR = "AAP2";
  public static final String AAP6_KLR = "AAP6";
  public static final String AAPR_KLR = "AAPR";
  public static final String AAAA_KLR = "AAAA";
  public static final String AAON_KLR = "AAON";
  public static final String AAD3_KLR = "AAD3";
  public static final String AABL_KLR = "AABL";
  public static final String AAFL_KLR = "AAFL";
  public static final String KLR_VERSION_01 = "01";
  public static final String DATE_FORMAT = "yyyy-MM-dd";
  public static final String BLANK = " ";
  public static final String NONSTOPDIRECT = "NonStopDirect";
  public static final String SINGLECONNECTION = "SingleConnection";
  public static final String DOUBLECONNECTION = "DoubleConnection";
  public static final String TRIPLECONNECTION = "TripleConnection";
  public static final String AVAILABILITY_HOST_BINDING = "config/flightSpecific_HostBinding.xml";
  public static final int CONSTANT_12 = 12;
  public static final int ERROR_CODE_400 = 400;
  public static final int ERROR_CODE_500 = 500;
  public static final int STATUS_CODE_200 = 200;
  public static final int CORE_AFFINITY_LENGTH_2 = 2;
  public static final int PLUS_DAYS_365 = 365;
  public static final int SUB_STRING_8 = 8;
  public static final int SUB_STRING_11 = 11;
  public static final int CONSTANT_10 = 10;
  public static final int CONSTANT_2 = 2;
  public static final int DAYS_IN_WEEK_7 = 7;
  public static final int BYTES_1024 = 1024;
  public static final String MORE_TOKEN = "OLD";
}
